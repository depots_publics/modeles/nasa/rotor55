# NASA rotor 55 blade model

## Key characteristics
- nominal speed: $`\Omega_\text{n}=840`$ rad/s
- Craig-Bampton [1,2] reduction parameter: $`\eta=10`$

## Content 
- CAD file
- universal (.unv) finite element mesh - quadratic pentahedron elements
- aster (.med) finite element mesh - quadratic pentahedron elements
- reduced order model (.mat)

## Details on the reduced order model
- blade root is clamped
- stiffness matrix (0 rad/s): $`\mathbf{k}`$
- [optional] centrifugal stiffness matrices: $`\mathbf{k}_1`$, $`\mathbf{k}_2`$ so that $`\mathbf{k}(\Omega)`$ = $`\mathbf{k}`$ + $`\mathbf{k}_1`$.$`\Omega^2`$ + $`\mathbf{k}_2`$.$`\Omega^4`$, see [3]
- mass matrix: $`\mathbf{m}`$
- organization of ddl [4]: 
  - first 24 dof: contact nodes along blade tip ($`1,2,3 = x,y,z`$ leading edge... $`22,23,24 = x,y,z`$ trailing edge)
  - remaining dof: modal dof related to Craig-Bampton reduction (up to $`3\eta`$ when centrifugal effects are accounted for)

## Material properties and more details 

https://lava-wiki.meca.polymtl.ca/public/modeles/rotor_55/accueil

## References
- [1] R.-R. Craig, M. C. C. Bampton, Coupling of Substructures for Dynamics Analyses, AIAA Journal. 1968, 6(7), pp. 1313–1319.
- [2] A. Batailly, M. Legrand, A. Millecamps, F. Garcin, Numerical-experimental comparison in the simulation of rotor/stator interaction through blade-tip/abradable coating contact, Journal of Engineering for Gas Turbines and Power 134 (8), 2012, 082504–01–11, https://hal.archives-ouvertes.fr/hal-00746632 - https://dx.doi.org/10.1115/1.4006446
- [3] A. Sternchüss, E. Balmés, On the reduction of quasi-cyclic disks with variable rotation speeds, in: Proceedings of the International Conference on Advanced Acoustics and Vibration Engineering (ISMA), 2006, pp. 3925–3939.
- [4] M. Legrand, A. Batailly, B. Magnain, P. Cartraud, C. Pierre, Full three-dimensional investigation of structural contact interactions in turbomachines, Journal of Sound and Vibration, 331 (11), 2012, pp. 2578–2601, https://hal.archives-ouvertes.fr/hal-00660863v3 - https://doi.org/10.1016/j.jsv.2012.01.017


